def Fzip (*args):
    res = []
    for j in range(len(max(args))):
        list = []
        for i in range(len(args)):
            list.append(args[i][j])
        res.append(list)
    return res


def Fmap (f,*args):
    z = Fzip(*args)
    res = []
    for i in z:
        res.append(f(*i))
    return res



def Ffilter (f,iterable):
    res = []
    for i in iterable:
        if f(i):
            res.append(i)
    return res



list1 = [i for i in range(10)]
list2 = [i for i in range(5,15)]
list3 = [i for i in range(10,20)]

print(Fzip(list3,list2,list1))
print(Fmap(lambda *x: (sum(x)),list1,list2,list3))
print([*(Ffilter(lambda x:x%2, list1))])